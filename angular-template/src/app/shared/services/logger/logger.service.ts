import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable()
export class LoggerService {

  constructor() { }

  log(object: any): void {
    if(!environment.production) {
      console.log(object);
    }
  }

  info(object: any): void {
    if(!environment.production) {
      console.info(object);
    }
  }

  error(object: any): void {
    if(!environment.production) {
      console.error(object);
    }
  }
}